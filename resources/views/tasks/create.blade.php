@extends('layout')

@section('content')
	 
@include('errors')

<div class="container">
<h3>Your responses</h3>

<div class="row">
<div class="col-md-12">
<select class="form-control" id="" name='title'>
<option>1%</option>
<option>2%</option>
<option>3%</option>
<option>4%</option>
<option>5%</option>
</select>
<form action="{{route('tasks.store')}}" method="get" value="{{old('title')}}"> 

<input type="text" class="form-control" name="title">
<br>
<textarea name="description" id="" cols="30" rows="10" class="
form-control">{{old('description')}}</textarea>
<br>
<button class="btn btn-success">Submit Response</button>
</form>
</div>
</div>
</div>
