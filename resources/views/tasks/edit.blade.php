@extends('layout')

@section('content')
	 
@include('errors')

<div class="container">
<h3>Edit task # - {{$task->id}}</h3>

<div class="row">
<div class="col-md-12">
<form action="{{route('tasks.update', $task->id)}}" method="get" value="{{$task->title}}"> 

<input type="text" class="form-control" name="title">
<br>
<textarea name="description" id="" cols="30" rows="10" class="
form-control">{{$task->description}}</textarea>
<br>
<button class="btn btn-warning">Submit</button>
</form>
</div>
</div>
</div>
