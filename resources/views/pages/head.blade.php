
<!DOCTYPE HTML>
<html>
<head>
<title>index</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />

<script type='text/javascript' src="js/jquery-1.11.1.min.js"></script>

<link href="css/style.css" rel='stylesheet' type='text/css' />

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Gretong Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,900' rel='stylesheet' type='text/css'>

<link href="css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="js/megamenu.js"></script>
<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
<script src="js/menu_jquery.js"></script>
<script src="js/simpleCart.min.js"> </script>
</head>
<body>

<div class="top_bg">
	<div class="container">
		<div class="header_top">
			<div class="top_right">
				
			</div>
			<div class="top_left">
				<h2><span></span> Круглосуточно : +7778 744 9217</h2>
			</div>
				<div class="clearfix"> </div>
		</div>
	</div>
</div>
<!-- header -->
<div class="header_bg">
<div class="container">
	<div class="header">
	<div class="head-t">
		<div class="logo">
			<a href="index.html"><img src="images/logow.jpg" class="img-responsive" alt=""/> </a>
		</div>
		
		<div class="header_right">
			<div class="rgt-bottom">
				<div class="log">
					<div class="login" >
						<div id="loginContainer"><a href="#" id="loginButton"><span>Вход</span></a>
						    <div id="loginBox">                
						        <form id="loginForm">
						                <fieldset id="body">
						                	<fieldset>
						                          <label for="email">Email Address</label>
						                          <input type="text" name="email" id="email">
						                    </fieldset>
						                    <fieldset>
						                            <label for="password">Password</label>
						                            <input type="password" name="password" id="password">
						                     </fieldset>
						                    <input type="submit" id="login" value="Войти">
						                	<label for="checkbox"><input type="checkbox" id="checkbox"> <i>Запомнить</i></label>
						            	</fieldset>
						            <span><a href="#">Забыли пароль?</a></span>
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="reg">
					<a href="">Регистрация</a>
				</div>
			<div class="cart box_1">
				<a href="checkout.html">
					<h3> <span class="simpleCart_total">$0.00</span> (<span id="simpleCart_quantity" class="simpleCart_quantity">0</span> items)<img src="images/bag.png" alt=""></h3>
				</a>	
				<p><a href="javascript:;" class="simpleCart_empty">(пустая карта)</a></p>
				<div class="clearfix"> </div>
			</div>
			<div class="create_btn">
				<a href="">CHECKOUT</a>
			</div>
			<div class="clearfix"> </div>
		</div>
		<div class="search">
		    <form>
		    	<input type="text" value="" placeholder="Поиск">
				<input type="submit" value="">
			</form>
		</div>
		<div class="clearfix"> </div>
		</div>
		<div class="clearfix"> </div>
	</div>
		<!-- start header menu -->
		<ul class="megamenu skyblue">
	
			<li class="grid"><a class="color2" href="#">Новое</a>
				<div class="megapanel">
					<div class="row">
						<div class="col1">
							<div class="h_nav">
								<h4>Clothing</h4>
								<ul>
									<li><a href="details.html">new arrivals</a></li>
									<li><a href="details.html">men</a></li>
									<li><a href="details.html">women</a></li>
									<li><a href="details.html">accessories</a></li>
									<li><a href="details.html">kids</a></li>
									<li><a href="details.html">brands</a></li>
								</ul>	
							</div>							
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>kids</h4>
								<ul>
									<li><a href="details.html">Pools&Tees</a></li>
									<li><a href="details.html">shirts</a></li>
									<li><a href="details.html">shorts</a></li>
									<li><a href="details.html">twinsets</a></li>
									<li><a href="details.html">kurts</a></li>
									<li><a href="details.html">jackets</a></li>
								</ul>	
							</div>							
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>Bags</h4>
								<ul>
									<li><a href="details.html">Handbag</a></li>
									<li><a href="details.html">Slingbags</a></li>
									<li><a href="details.html">Clutches</a></li>
									<li><a href="details.html">Totes</a></li>
									<li><a href="details.html">Wallets</a></li>
									<li><a href="details.html">Laptopbags</a></li>
								</ul>	
							</div>												
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>account</h4>
								<ul>
									<li><a href="#">login</a></li>
									<li><a href="">create an account</a></li>
									<li><a href="details.html">create wishlist</a></li>
									<li><a href="details.html">my shopping bag</a></li>
									<li><a href="details.html">brands</a></li>
									<li><a href="details.html">create wishlist</a></li>
								</ul>	
							</div>						
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>Accessories</h4>
								<ul>
									<li><a href="details.html">Belts</a></li>
									<li><a href="details.html">Pens</a></li>
									<li><a href="details.html">Eyeglasses</a></li>
									<li><a href="details.html">accessories</a></li>
									<li><a href="details.html">Watches</a></li>
									<li><a href="details.html">Jewellery</a></li>
								</ul>	
							</div>
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>Footwear</h4>
								<ul>
									<li><a href="details.html">new arrivals</a></li>
									<li><a href="details.html">men</a></li>
									<li><a href="details.html">women</a></li>
									<li><a href="details.html">accessories</a></li>
									<li><a href="details.html">kids</a></li>
									<li><a href="details.html">style videos</a></li>
								</ul>	
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col2"></div>
						<div class="col1"></div>
						<div class="col1"></div>
						<div class="col1"></div>
						<div class="col1"></div>
					</div>
    				</div>
				</li>
			<li><a class="color4" href="#">Смокинг</a>
				<div class="megapanel">
					<div class="row">
						<div class="col1">
							<div class="h_nav">
								<h4>Clothing</h4>
								<ul>
									<li><a href="details.html">new arrivals</a></li>
									<li><a href="details.html">men</a></li>
									<li><a href="details.html">women</a></li>
									<li><a href="details.html">accessories</a></li>
									<li><a href="details.html">kids</a></li>
									<li><a href="details.html">brands</a></li>
								</ul>	
							</div>							
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>kids</h4>
								<ul>
									<li><a href="details.html">Pools&Tees</a></li>
									<li><a href="details.html">shirts</a></li>
									<li><a href="details.html">shorts</a></li>
									<li><a href="details.html">twinsets</a></li>
									<li><a href="details.html">kurts</a></li>
									<li><a href="details.html">jackets</a></li>
								</ul>	
							</div>							
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>Bags</h4>
								<ul>
									<li><a href="details.html">Handbag</a></li>
									<li><a href="details.html">Slingbags</a></li>
									<li><a href="details.html">Clutches</a></li>
									<li><a href="details.html">Totes</a></li>
									<li><a href="details.html">Wallets</a></li>
									<li><a href="details.html">Laptopbags</a></li>
								</ul>	
							</div>												
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>account</h4>
								<ul>
									<li><a href="#">login</a></li>
									<li><a href="">create an account</a></li>
									<li><a href="details.html">create wishlist</a></li>
									<li><a href="details.html">my shopping bag</a></li>
									<li><a href="details.html">brands</a></li>
									<li><a href="details.html">create wishlist</a></li>
								</ul>	
							</div>						
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>Accessories</h4>
								<ul>
									<li><a href="details.html">Belts</a></li>
									<li><a href="details.html">Pens</a></li>
									<li><a href="details.html">Eyeglasses</a></li>
									<li><a href="details.html">accessories</a></li>
									<li><a href="details.html">Watches</a></li>
									<li><a href="details.html">Jewellery</a></li>
								</ul>	
							</div>
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>Footwear</h4>
								<ul>
									<li><a href="details.html">new arrivals</a></li>
									<li><a href="details.html">men</a></li>
									<li><a href="details.html">women</a></li>
									<li><a href="details.html">accessories</a></li>
									<li><a href="details.html">kids</a></li>
									<li><a href="details.html">style videos</a></li>
								</ul>	
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col2"></div>
						<div class="col1"></div>
						<div class="col1"></div>
						<div class="col1"></div>
						<div class="col1"></div>
					</div>
    				</div>
				</li>				
				<li><a class="color5" href="#">Свитер</a>
				<div class="megapanel">
					<div class="row">
						<div class="col1">
							<div class="h_nav">
								<h4>Clothing</h4>
								<ul>
									<li><a href="details.html">new arrivals</a></li>
									<li><a href="details.html">men</a></li>
									<li><a href="details.html">women</a></li>
									<li><a href="details.html">accessories</a></li>
									<li><a href="details.html">kids</a></li>
									<li><a href="details.html">brands</a></li>
								</ul>	
							</div>							
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>kids</h4>
								<ul>
									<li><a href="details.html">Pools&Tees</a></li>
									<li><a href="details.html">shirts</a></li>
									<li><a href="details.html">shorts</a></li>
									<li><a href="details.html">twinsets</a></li>
									<li><a href="details.html">kurts</a></li>
									<li><a href="details.html">jackets</a></li>
								</ul>	
							</div>							
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>Bags</h4>
								<ul>
									<li><a href="details.html">Handbag</a></li>
									<li><a href="details.html">Slingbags</a></li>
									<li><a href="details.html">Clutches</a></li>
									<li><a href="details.html">Totes</a></li>
									<li><a href="details.html">Wallets</a></li>
									<li><a href="details.html">Laptopbags</a></li>
								</ul>	
							</div>												
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>account</h4>
								<ul>
									<li><a href="#">login</a></li>
									<li><a href="register.html">create an account</a></li>
									<li><a href="details.html">create wishlist</a></li>
									<li><a href="details.html">my shopping bag</a></li>
									<li><a href="details.html">brands</a></li>
									<li><a href="details.html">create wishlist</a></li>
								</ul>	
							</div>						
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>Accessories</h4>
								<ul>
									<li><a href="details.html">Belts</a></li>
									<li><a href="details.html">Pens</a></li>
									<li><a href="details.html">Eyeglasses</a></li>
									<li><a href="details.html">accessories</a></li>
									<li><a href="details.html">Watches</a></li>
									<li><a href="details.html">Jewellery</a></li>
								</ul>	
							</div>
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>Footwear</h4>
								<ul>
									<li><a href="details.html">new arrivals</a></li>
									<li><a href="details.html">men</a></li>
									<li><a href="details.html">women</a></li>
									<li><a href="details.html">accessories</a></li>
									<li><a href="details.html">kids</a></li>
									<li><a href="details.html">style videos</a></li>
								</ul>	
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col2"></div>
						<div class="col1"></div>
						<div class="col1"></div>
						<div class="col1"></div>
						<div class="col1"></div>
					</div>
    				</div>
				</li>
				<li><a class="color6" href="#">ОБУВЬ</a>
				<div class="megapanel">
					<div class="row">
						<div class="col1">
							<div class="h_nav">
								<h4>Clothing</h4>
								<ul>
									<li><a href="details.html">new arrivals</a></li>
									<li><a href="details.html">men</a></li>
									<li><a href="details.html">women</a></li>
									<li><a href="details.html">accessories</a></li>
									<li><a href="details.html">kids</a></li>
									<li><a href="details.html">brands</a></li>
								</ul>	
							</div>							
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>kids</h4>
								<ul>
									<li><a href="details.html">Pools&Tees</a></li>
									<li><a href="details.html">shirts</a></li>
									<li><a href="details.html">shorts</a></li>
									<li><a href="details.html">twinsets</a></li>
									<li><a href="details.html">kurts</a></li>
									<li><a href="details.html">jackets</a></li>
								</ul>	
							</div>							
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>Bags</h4>
								<ul>
									<li><a href="details.html">Handbag</a></li>
									<li><a href="details.html">Slingbags</a></li>
									<li><a href="details.html">Clutches</a></li>
									<li><a href="details.html">Totes</a></li>
									<li><a href="details.html">Wallets</a></li>
									<li><a href="details.html">Laptopbags</a></li>
								</ul>	
							</div>												
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>account</h4>
								<ul>
									<li><a href="#">login</a></li>
									<li><a href="register.html">create an account</a></li>
									<li><a href="details.html">create wishlist</a></li>
									<li><a href="details.html">my shopping bag</a></li>
									<li><a href="details.html">brands</a></li>
									<li><a href="details.html">create wishlist</a></li>
								</ul>	
							</div>						
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>Accessories</h4>
								<ul>
									<li><a href="details.html">Belts</a></li>
									<li><a href="details.html">Pens</a></li>
									<li><a href="details.html">Eyeglasses</a></li>
									<li><a href="details.html">accessories</a></li>
									<li><a href="details.html">Watches</a></li>
									<li><a href="details.html">Jewellery</a></li>
								</ul>	
							</div>
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>Footwear</h4>
								<ul>
									<li><a href="details.html">new arrivals</a></li>
									<li><a href="details.html">men</a></li>
									<li><a href="details.html">women</a></li>
									<li><a href="details.html">accessories</a></li>
									<li><a href="details.html">kids</a></li>
									<li><a href="details.html">style videos</a></li>
								</ul>	
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col2"></div>
						<div class="col1"></div>
						<div class="col1"></div>
						<div class="col1"></div>
						<div class="col1"></div>
					</div>
    				</div>
				</li>				
			
				<li><a class="color7" href="#">Очки</a>
				<div class="megapanel">
					<div class="row">
						<div class="col1">
							<div class="h_nav">
								<h4>Clothing</h4>
								<ul>
									<li><a href="details.html">new arrivals</a></li>
									<li><a href="details.html">men</a></li>
									<li><a href="details.html">women</a></li>
									<li><a href="details.html">accessories</a></li>
									<li><a href="details.html">kids</a></li>
									<li><a href="details.html">brands</a></li>
								</ul>	
							</div>							
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>kids</h4>
								<ul>
									<li><a href="details.html">Pools&Tees</a></li>
									<li><a href="details.html">shirts</a></li>
									<li><a href="details.html">shorts</a></li>
									<li><a href="details.html">twinsets</a></li>
									<li><a href="details.html">kurts</a></li>
									<li><a href="details.html">jackets</a></li>
								</ul>	
							</div>							
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>Bags</h4>
								<ul>
									<li><a href="details.html">Handbag</a></li>
									<li><a href="details.html">Slingbags</a></li>
									<li><a href="details.html">Clutches</a></li>
									<li><a href="details.html">Totes</a></li>
									<li><a href="details.html">Wallets</a></li>
									<li><a href="details.html">Laptopbags</a></li>
								</ul>	
							</div>												
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>account</h4>
								<ul>
									<li><a href="#">login</a></li>
									<li><a href="register.html">create an account</a></li>
									<li><a href="details.html">create wishlist</a></li>
									<li><a href="details.html">my shopping bag</a></li>
									<li><a href="details.html">brands</a></li>
									<li><a href="details.html">create wishlist</a></li>
								</ul>	
							</div>						
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>Accessories</h4>
								<ul>
									<li><a href="details.html">Belts</a></li>
									<li><a href="details.html">Pens</a></li>
									<li><a href="details.html">Eyeglasses</a></li>
									<li><a href="details.html">accessories</a></li>
									<li><a href="details.html">Watches</a></li>
									<li><a href="details.html">Jewellery</a></li>
								</ul>	
							</div>
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>Footwear</h4>
								<ul>
									<li><a href="details.html">new arrivals</a></li>
									<li><a href="details.html">men</a></li>
									<li><a href="details.html">women</a></li>
									<li><a href="details.html">accessories</a></li>
									<li><a href="details.html">kids</a></li>
									<li><a href="details.html">style videos</a></li>
								</ul>	
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col2"></div>
						<div class="col1"></div>
						<div class="col1"></div>
						<div class="col1"></div>
						<div class="col1"></div>
					</div>
    				</div>
				</li>				
			
				<li><a class="color8" href="#">Футболки</a>
				<div class="megapanel">
					<div class="row">
						<div class="col1">
							<div class="h_nav">
								<h4>Clothing</h4>
								<ul>
									<li><a href="details.html">new arrivals</a></li>
									<li><a href="details.html">men</a></li>
									<li><a href="details.html">women</a></li>
									<li><a href="details.html">accessories</a></li>
									<li><a href="details.html">kids</a></li>
									<li><a href="details.html">brands</a></li>
								</ul>	
							</div>							
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>kids</h4>
								<ul>
									<li><a href="details.html">trends</a></li>
									<li><a href="details.html">sale</a></li>
									<li><a href="details.html">style videos</a></li>
									<li><a href="details.html">accessories</a></li>
									<li><a href="details.html">kids</a></li>
									<li><a href="details.html">style videos</a></li>
								</ul>	
							</div>							
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>Bags</h4>
								<ul>
									<li><a href="details.html">trends</a></li>
									<li><a href="details.html">sale</a></li>
									<li><a href="details.html">style videos</a></li>
									<li><a href="details.html">accessories</a></li>
									<li><a href="details.html">kids</a></li>
									<li><a href="details.html">style videos</a></li>
								</ul>	
							</div>												
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>account</h4>
								<ul>
									<li><a href="#">login</a></li>
									<li><a href="">create an account</a></li>
									<li><a href="details.html">create wishlist</a></li>
									<li><a href="details.html">my shopping bag</a></li>
									<li><a href="details.html">brands</a></li>
									<li><a href="details.html">create wishlist</a></li>
								</ul>	
							</div>						
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>Accessories</h4>
								<ul>
									<li><a href="details.html">trends</a></li>
									<li><a href="details.html">sale</a></li>
									<li><a href="details.html">style videos</a></li>
									<li><a href="details.html">accessories</a></li>
									<li><a href="details.html">kids</a></li>
									<li><a href="details.html">style videos</a></li>
								</ul>	
							</div>
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>Footwear</h4>
								<ul>
									<li><a href="details.html">new arrivals</a></li>
									<li><a href="details.html">men</a></li>
									<li><a href="details.html">women</a></li>
									<li><a href="details.html">accessories</a></li>
									<li><a href="details.html">kids</a></li>
									<li><a href="details.html">style videos</a></li>
								</ul>	
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col2"></div>
						<div class="col1"></div>
						<div class="col1"></div>
						<div class="col1"></div>
						<div class="col1"></div>
					</div>
    				</div>
				</li>
				<li><a class="color9" href="#">ЧАСЫ</a>
				<div class="megapanel">
					<div class="row">
						<div class="col1">
							<div class="h_nav">
								<h4>Clothing</h4>
								<ul>
									<li><a href="details.html">new arrivals</a></li>
									<li><a href="details.html">men</a></li>
									<li><a href="details.html">women</a></li>
									<li><a href="details.html">accessories</a></li>
									<li><a href="details.html">kids</a></li>
									<li><a href="details.html">brands</a></li>
								</ul>	
							</div>							
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>kids</h4>
								<ul>
									<li><a href="details.html">trends</a></li>
									<li><a href="details.html">sale</a></li>
									<li><a href="details.html">style videos</a></li>
									<li><a href="details.html">accessories</a></li>
									<li><a href="details.html">kids</a></li>
									<li><a href="details.html">style videos</a></li>
								</ul>	
							</div>							
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>Bags</h4>
								<ul>
									<li><a href="details.html">trends</a></li>
									<li><a href="details.html">sale</a></li>
									<li><a href="details.html">style videos</a></li>
									<li><a href="details.html">accessories</a></li>
									<li><a href="details.html">kids</a></li>
									<li><a href="details.html">style videos</a></li>
								</ul>	
							</div>												
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>account</h4>
								<ul>
									<li><a href="#">login</a></li>
									<li><a href="">create an account</a></li>
									<li><a href="details.html">create wishlist</a></li>
									<li><a href="details.html">my shopping bag</a></li>
									<li><a href="details.html">brands</a></li>
									<li><a href="details.html">create wishlist</a></li>
								</ul>	
							</div>						
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>Accessories</h4>
								<ul>
									<li><a href="details.html">trends</a></li>
									<li><a href="details.html">sale</a></li>
									<li><a href="details.html">style videos</a></li>
									<li><a href="details.html">accessories</a></li>
									<li><a href="details.html">kids</a></li>
									<li><a href="details.html">style videos</a></li>
								</ul>	
							</div>
						</div>
						<div class="col1">
							<div class="h_nav">
								<h4>Footwear</h4>
								<ul>
									<li><a href="details.html">new arrivals</a></li>
									<li><a href="details.html">men</a></li>
									<li><a href="details.html">women</a></li>
									<li><a href="details.html">accessories</a></li>
									<li><a href="details.html">kids</a></li>
									<li><a href="details.html">style videos</a></li>
								</ul>	
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col2"></div>
						<div class="col1"></div>
						<div class="col1"></div>
						<div class="col1"></div>
						<div class="col1"></div>
					</div>
    				</div>
				</li>
		 </ul> 
	</div>
</div>
</div>
<div class="arriv">
	<div class="container">
		<div class="arriv-top">
			<div class="col-md-6 arriv-left">
				<img src="images/12.jpg" class="img-responsive" alt="">
				<div class="arriv-info">
					<h3>НОВЫЕ ПОСТУПЛЕНИЯ</h3>
					<p>    ВОССТАНОВИТЕ СВОЙ ГАРДЕРОБ</p>
					<div class="crt-btn">
						<a href="details.html">СМОТРЕТЬ</a>
					</div>
				</div>
			</div>
			<div class="col-md-6 arriv-right">
				<img src="images/2.jpg" class="img-responsive" alt="">
				<div class="arriv-info">
					<h3>СМОКИНГ</h3>
					<p>ВОССТАНОВИТЕ СВОЙ ГАРДЕРОБ</p>
					<div class="crt-btn">
						<a href="details.html">КУПИТЬ СЕЙЧАС</a>
					</div>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
		<div class="arriv-bottm">
			<div class="col-md-8 arriv-left1">
				<img src="images/3.jpg" class="img-responsive" alt="">
				<div class="arriv-info1">
					<h3>Свитер</h3>
					<p>ВОССТАНОВИТЕ СВОЙ ГАРДЕРОБ</p>
					<div class="crt-btn">
						<a href="details.html">КУПИТЬ СЕЙЧАС</a>
					</div>
				</div>
			</div>
			<div class="col-md-4 arriv-right1">
				<img src="images/4.jpg" class="img-responsive" alt="">
				<div class="arriv-info2">
					<a href="details.html"><h3>Обувь Похода<i class="ars"></i></h3></a>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
		<div class="arriv-las">
			<div class="col-md-4 arriv-left2">
				<img src="images/5.jpg" class="img-responsive" alt="">
				<div class="arriv-info2">
					<a href="details.html"><h3>Случайные очки<i class="ars"></i></h3></a>
				</div>
			</div>
			<div class="col-md-4 arriv-middle">
				<img src="images/6.jpg" class="img-responsive" alt="">
				<div class="arriv-info3">
					<h3>Новые Футболки</h3>
					<div class="crt-btn">
						<a href="details.html">КУПИТЬ СЕЙЧАС</a>
					</div>
				</div>
			</div>
			<div class="col-md-4 arriv-right2">
				<img src="images/7.jpg" class="img-responsive" alt="">
				<div class="arriv-info2">
					<a href="details.html"><h3>ИЗЯЩНЫЕ ЧАСЫ<i class="ars"></i></h3></a>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<div class="special">
	<div class="container">
		<h3>Особые вещи</h3>
		<div class="specia-top">
			<ul class="grid_2">
		<li>
				<a href="details.html"><img src="images/8.jpg" class="img-responsive" alt=""></a>
				<div class="special-info grid_1 simpleCart_shelfItem">
					<h5>Shadow</h5>
					<div class="item_add"><span class="item_price"><h6>ВСЕГО ЛИШЬ $40.00</h6></span></div>
					<div class="item_add"><span class="item_price"><a href="#">добавить в карту</a></span></div>
				</div>
		</li>
		<li>
				<a href="details.html"><img src="images/9.jpg" class="img-responsive" alt=""></a>
				<div class="special-info grid_1 simpleCart_shelfItem">
					<h5>B - SWISS</h5>
					<div class="item_add"><span class="item_price"><h6>ВСЕГО ЛИШЬ $60.00</h6></span></div>
					<div class="item_add"><span class="item_price"><a href="#">добавить в карту</a></span></div>
			</div>
		</li>
		<li>
				<a href="details.html"><img src="images/10.jpg" class="img-responsive" alt=""></a>
				<div class="special-info grid_1 simpleCart_shelfItem">
					<h5>RAY BAN</h5>
					<div class="item_add"><span class="item_price"><h6>ВСЕГО ЛИШЬ $14.00</h6></span></div>
					<div class="item_add"><span class="item_price"><a href="#">добавить в карту</a></span></div>
			</div>
		</li>
		<li>
				<a href="details.html"><img src="images/11.jpg" class="img-responsive" alt=""></a>
				<div class="special-info grid_1 simpleCart_shelfItem">
					<h5>Terro - pro</h5>
					<div class="item_add"><span class="item_price"><h6>ВСЕГО ЛИШЬ $37.00</h6></span></div>
					<div class="item_add"><span class="item_price"><a href="#">добавить в карту</a></span></div>
				</div>
		</li>
		<div class="clearfix"> </div>
	</ul>
		</div>
	</div>
</div>
<div class="foot-top">
	<div class="container">
		<div class="col-md-6 s-c">
			<li>
				<div class="fooll">
					<h5>Следите за нами в</h5>
				</div>
			</li>
			<li>
				<div class="social-ic">
					<ul>
						<li><a href="#"><i class="facebok"> </i></a></li>
						<li><a href="#"><i class="twiter"> </i></a></li>
						<li><a href="#"><i class="goog"> </i></a></li>
						<li><a href="#"><i class="be"> </i></a></li>
						<li><a href="#"><i class="pp"> </i></a></li>
							<div class="clearfix"></div>	
					</ul>
				</div>
			</li>
				<div class="clearfix"> </div>
		</div>
		<div class="col-md-6 s-c">
			<div class="stay">
						<div class="stay-left">
							<form>
								<input type="text" placeholder="Войдите в свою электронную почту" required="">
							</form>
						</div>
						<div class="btn-1">
							<form>
								<input type="submit" value="join">
							</form>
						</div>
							<div class="clearfix"> </div>
			</div>
		</div>
			<div class="clearfix"> </div>
	</div>
</div>
<div class="footer">
	<div class="container">
		<div class="col-md-3 cust">
			<h4>Работа с клиентами</h4>
				<li><a href="#">Справочный центр</a></li>
				<li><a href="#">Вопросы</a></li>
				<li><a href="">Как купить</a></li>
				<li><a href="#">Поступления</a></li>
		</div>
		<div class="col-md-2 abt">
			<h4>О нас</h4>
				<li><a href="#">Наша история</a></li>
				<li><a href="#">Печати</a></li>
				<li><a href="#">Карьера</a></li>
				<li><a href="">Конакт</a></li>
		</div>
		<div class="col-md-2 myac">
			<h4>Наш Аккаунт</h4>
				<li><a href="">Регистер</a></li>
				<li><a href="#">Моя карта</a></li>
				<li><a href="#">История заказов</a></li>
				<li><a href="">Платежи</a></li>
		</div>
		<div class="col-md-5 our-st">
			<div class="our-left">
				<h4>Наш магазин</h4>
			</div>
			<div class="our-left1">
				<div class="cr_btn">
					<a href="#">SOLO</a>
				</div>
			</div>
			<div class="our-left1">
				<div class="cr_btn1">
					<a href="#">BOGOR</a>
				</div>
			</div>
			<div class="clearfix"> </div>
				<li><i class="add"> </i>Жандосова/Манаса</li>
				<li><i class="phone"> </i>025-2839341</li>
				<li><a href="mailto:info@example.com"><i class="mail"> </i>webshop@gmail.com </a></li>
			
		</div>
		<div class="clearfix"> </div>
			<p>Главное, чтобы Вы были счастливы!</a></p>
	</div>
</div>
</body>
</html>